from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException as timeout_exception
from selenium.webdriver.support.ui import WebDriverWait as web_driver_wait
from selenium.webdriver.support import expected_conditions as EC

MAX_ITERATIONS = 900;
SECONDS_IN_A_MINUTE = 60;

USER_NAME = "";
PASSWORD = "";
driver = webdriver.Chrome(ChromeDriverManager().install());
driver.get("https://www.instagram.com/");

def delay_web(delay_seconds, driver_e):
	try:
	    commentr = web_driver_wait(driver_e, delay_seconds).until(EC.element_to_be_clickable( (By.ID, 'contenteditable-textarea') ));
	    print("Page is ready!");
	except timeout_exception:
	    print("");

def get_names_from_list_users():
	names = [];
	f = open("listUsers.txt", "r");
	for x in f:
		names.append(x.replace("\n",""));
	return names;

def is_this_user_following_me(user_name, driver_e):
		print(user_name);
		user = driver_e.find_element_by_xpath("//a[@href='/"+user_name+"/']");
		user.click();

		delay_web(2, driver_e);
		user_followers = driver_e.find_element_by_xpath("//a[@href='/"+user_name+"/following/']");
		user_followers.click();

		delay_web(4, driver_e)
		list_followers = driver_e.find_element_by_xpath("//div[@class='isgrP']//ul[@class='_6xe7A']");
		itsMe = list_followers.text.split("\n")[0];
		print(itsMe);
		print(user_name);
		if itsMe == USER_NAME:
			print("follows me");
		else:
			print("does not follow");
			

		driver_e.back();
		delay_web(2, driver_e);
		driver_e.back();
		delay_web(2, driver_e);

names = get_names_from_list_users();

cookies_button = driver.find_element_by_xpath('//button[text()="Permitir solo cookies necesarias"]');
cookies_button.click();

delay_web(3, driver);

user_input = driver.find_element_by_name('username');
psw_input = driver.find_element_by_name('password');

user_input.clear();
psw_input.clear();

user_input.send_keys(USER_NAME);
psw_input.send_keys(PASSWORD);

login_button = driver.find_element_by_xpath("//button[@type='submit']");
login_button.click();

delay_web(6, driver);
login_button = driver.find_element_by_xpath("//span[@class='_2dbep qNELH']");
login_button.click();

login_button = driver.find_element_by_xpath("//a[@class='-qQT3']");
login_button.click();

delay_web(4, driver);
login_button = driver.find_element_by_xpath("//a[@href='/alvarito_lago/following/']");
login_button.click();

delay_web(4, driver);
unfollowed_users = 0;

for i in range(MAX_ITERATIONS):
	li_element = driver.find_elements_by_xpath("//div[@class='isgrP']//ul[@class='_6xe7A']//li");
	try:
		user_name = li_element[i].text.split("\n")[0];
	except:
		delay_web(10, driver);
		user_name = li_element[i].text.split("\n")[0];
	print(user_name);
	print(i);
	print("unfollowed_users:");
	print(unfollowed_users);
	delay_web(1, driver);

	if unfollowed_users % 15 == 0 and unfollowed_users != 0:
		delay_web(2 * SECONDS_IN_A_MINUTE, driver);

	if user_name not in names:
		print("UNFOLLOW");
		unfollowed_users += 1;
		button_unfollow = li_element[i].find_elements_by_xpath("//div[@class='uu6c_']//div[@class='Pkbci']//button");
		button_unfollow[i].click();
		delay_web(0.5, driver);
		driver.find_element_by_xpath("//button[@class='aOOlW -Cab_   ']").click();
		delay_web(30, driver);

	delay_web(1, driver);

	driver.execute_script("arguments[0].scrollIntoView();", li_element[i]);


delay = 200
try:
    commentr = web_driver_wait(driver,delay).until(EC.element_to_be_clickable( (By.ID, 'contenteditable-textarea') ));
    print("Page is ready!");
except timeout_exception:
    print("");

assert "No results found." not in driver.page_source
driver.close();